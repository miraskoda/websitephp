<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->


<!DOCTYPE html>
<html>
<head>

    <title>GPL CZ s.r.o.</title>
<!-- For-Mobile-Apps -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Cubicle a Responsive Web Template, Bootstrap Web Templates, Flat Web Templates, Android Compatible Web Template, Smartphone Compatible Web Template, Free Webdesigns for Nokia, Samsung, LG, Sony Ericsson, Motorola Web Design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- //For-Mobile-Apps -->

<!-- Custom-Theme-Files -->
	<!-- Bootstrap-Core-CSS --> <link href="https://getbootstrap.com/docs/3.3/dist/css/bootstrap.min.css" rel="stylesheet">
	<!-- Style.CSS --> <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- Owl-Carousel-CSS --> <link href="css/owl.carousel.css" rel="stylesheet">
	<!-- Swipe-Box-CSS --> <link rel="stylesheet" href="css/swipebox.css">
<!-- //Custom-Theme-Files -->
<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>

    <script>
        $(function() {
            $(".meter > span").each(function() {
                $(this)
                    .data("origWidth", $(this).width())
                    .width(0)
                    .animate({
                        width: $(this).data("origWidth")
                    }, 1200);
            });
        });
    </script>

<!-- Web-Fonts -->
	<link href='//fonts.googleapis.com/css?family=Slabo+27px' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:400,500,600,700,800' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet' type='text/css'>
<!-- //Web-Fonts -->

</head>
<body>

	<!-- Header -->
	<div class="header">

		<div class="container">

			<!-- Navigation -->
			<nav class="navbar navbar-inverse navbar-default">

				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<!-- Logo -->
					<div class="logo">
                        <img class="mt-5" src="logo.png" >
					</div>

					<!-- //Logo -->
				</div>

				<div id="navbar" class="navbar-collapse navbar-right collapse">
					<ul class="nav navbar-nav navbar-right cross-effect" id="cross-effect">
						<li><a class="cross-effect" href="#">Servis</a></li>
						<li><a class="cross-effect" href="#">Výstavba</a></li>
						<li><a class="cross-effect" href="#">Produkty</a></li>
						<li><a class="cross-effect" href="#">Reference</a></li>
						<li><a class="cross-effect" href="#">Kontakty</a></li>
						<li><a class="cross-effect" href="#">Certifikace</a></li>

					</ul>
				</div><!-- //Navbar-Collapse -->

			</nav>
			<!-- //Navigation -->

		</div>
		<!-- //Container -->

		<!-- Carousel -->
		<div id="myCarousel" class="carousel slide" data-ride="carousel">

			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img class="first-slide" src="images/1.jpg" alt="Cubicle">
					<div class="slider-grid-bg">
						<div class="slider-grid-text">
                            <h5>Proč si máte vybrat právě nás?</h5>
                            <div class="border"></div>
                            <p> Nabízíme nonstop 24 hodin servisní služby
                                <br>
                                Dodáme vám stanici na klíč
                                <br>
                                Postaráme se o kolaudační řízení
                                <br>
                                Máme dlouhodobé zkušenosti v oboru čerpacích stanic
                            </p>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="second-slide" src="images/2.jpg" alt="Cubicle">
					<div class="slider-grid-bg">
						<div class="slider-grid-text">
							<h5>Svým zákazníkům můžeme nabídnout</h5>
							<div class="border"></div>
							<p>Dodávku, montáž a zprovoznění čerpacích stanic LPG a PHM
                            </p>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="third-slide" src="images/3.jpg" alt="Cubicle">
					<div class="slider-grid-bg">
						<div class="slider-grid-text">
							<h5>Svým zákazníkům můžeme nabídnout</h5>
							<div class="border"></div>
							<p>Zajištění dodávek LPG
                            </p>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="fourth-slide" src="images/4.jpg" alt="Cubicle">
					<div class="slider-grid-bg">
						<div class="slider-grid-text">
							<h5>Ve všech prováděných činnostech klademe důraz na kvalitu a odbornost</h5>
							<div class="border"></div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor.</p>
						</div>
					</div>
				</div>
				<div class="item">
					<img class="fifth-slide" src="images/5.jpg" alt="Cubicle">
					<div class="slider-grid-bg">
						<div class="slider-grid-text">

                            <h5>Svým zákazníkům můžeme nabídnout</h5>
                            <div class="border"></div>
                            <p>Vypracování projektové dokumentace technologií ke stavebnímu povolení
                            </p>

						</div>
					</div>
				</div>
			</div>

            <nav class="nav-diamond">
                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                    <span class="icon-wrap" aria-hidden="true"></span>
                    <div class="left-arrow">
                    </div>
                </a>
                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                    <span class="icon-wrap" aria-hidden="true"></span>
                    <div class="right-arrow">
                    </div>
                </a>
            </nav>

		</div>
		<!-- //Carousel -->

	</div>
	<!-- //Header -->



	<!-- Footer -->
	<div class="footer">
		<div class="container">

			<div class="footer-info slideanim">
				<div class="col-md-3 col-sm-3 footer-info-grid links">
					<h4>O nás</h4>
					<ul>
						<p>Firma GPL-CZ s.r.o. již 15. rokem působí na českém trhu. Svoji práci odvádíme kvalitně a bez kompromisů.</p>
					</ul>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid services">
					<h4>Reference</h4>
					<ul>
                        <p>Mezi naše dlouholeté partnery patří například Tank ono, Benzina, OMV.</p>

                    </ul>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid address">
					<h4>Adresa firmy</h4>
					<address>
						<ul>
							<li>GPL CZ s.r.o.</li>
							<li>Na Důchodě 1561/29</li>
							<li>Hradec Králové 500 02</li>
							<li>Tel 608 119 437, 608 323 585</li>
						</ul>
						<p>Email : <a class="mail" href="mailto:info@gpl-cz.cz">info@gpl-cz.cz</a></p>
					</address>
				</div>
				<div class="col-md-3 col-sm-3 footer-info-grid email">
					<h4>Novinky emailem</h4>
					<p>Nechte nám svůj email, budeme vám zasílat nejnovější nabídky a aktuality.
					</p>

					<form class="newsletter">
						<input class="email" type="email" placeholder="Váš email...">
						<input type="submit" class="submit" value="">
					</form>
				</div>
				<div class="clearfix"></div>
			</div>

			<div class="copyright slideanim">
                <p>written by <a href=http://www.miroslavskoda.cz><b>Miroslav Škoda</b></a> @2018</p>
			</div>

		</div> 
	</div>
	<!-- //Footer -->

<!-- Custom-JavaScript-File-Links -->
	<!-- Supportive-JavaScript --> <script type="text/javascript" src="js/jquery.min.js"></script>
	<!-- Necessary-JavaScript-File-For-Bootstrap --> <script type="text/javascript" src="js/bootstrap.min.js"></script>

	<!-- Slideanim-JavaScript -->
	<script type="text/javascript">
		$(window).scroll(function() {
	 		$(".slideanim").each(function(){
				var pos = $(this).offset().top;
				var winTop = $(window).scrollTop();
					if (pos < winTop + 600) {
					$(this).addClass("slide");
				}
			});
		});
	</script>
	<!-- //Slideanim-JavaScript -->

	<!-- Gallery-Tab-JavaScript -->
	<script src="js/cbpFWTabs.js"></script>
	<script>
		(function() {
			[].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
				new CBPFWTabs( el );
			});
		})();
	</script>
	<!-- //Gallery-Tab-JavaScript -->

	<!-- Swipe-Box-JavaScript -->
		<script src="js/jquery.swipebox.min.js"></script> 
			<script type="text/javascript">
				jQuery(function($) {
					$(".swipebox").swipebox();
				});
		</script>
	<!-- //Swipe-Box-JavaScript -->

	<!-- Owl-Carousel-JavaScript -->
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function() {
			$("#owl-demo").owlCarousel ({
				items : 8,
				lazyLoad : true,
				autoPlay : true,
				speed: 900,
				pagination : false,
			});
		});
	</script>
	<!-- //Owl-Carousel-JavaScript -->

	<!-- Slide-To-Top JavaScript (No-Need-To-Change) -->
	<script type="text/javascript">
		$(document).ready(function() {
			var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 100,
				easingType: 'linear'
			};
			$().UItoTop({ easingType: 'easeOutQuart' });
		});
	</script>
	<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 0;"> </span></a>
	<!-- //Slide-To-Top JavaScript -->

	<!-- Smooth-Scrolling-JavaScript -->
	<script type="text/javascript" src="js/move-top.js"></script>
	<script type="text/javascript" src="js/easing.js"></script>

	<!-- //Smooth-Scrolling-JavaScript -->

	<!-- Skills-Animation-JavaScript -->
	<script type="text/javascript" src="js/jquery.inview.min.js"></script>
	<script type="text/javascript" src="js/wow.min.js"></script>
	<script type="text/javascript" src="js/mousescroll.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<!-- //Skills-Animation-JavaScript -->

	<script type="text/javascript" src="js/numscroller-1.0.js"></script>


</body>
</html>