
<!-- Portfolio Item Heading -->

<h1 class="my-4">Kdo jsem?

</h1>



<!-- Portfolio Item Row -->

<div class="row">



    <div class="col-md-8">

        <img class="img-fluid" src="Img/me.jpg" alt="">

    </div>



    <div class="col-md-4">

        <h2>Ahoj!</h2><p>Jmenuji se Miroslav Škoda a jsem studentem druhého ročníku Univerzity Hradec Králové oboru Informatika a management. V minulosti až do součastnosti jsem se věnoval hlavně oblasti hardwaru. Software mi byl více vzdálený a tak jsem se tento handicap rozhodl změnit. Co na těchto stránkách najdete? Primárně je buduji za účelem představení sama sebe a také jako dlouhodobější projekt v rámci školy.</p>

    </div>



</div>

<!-- /.row -->



<div class="row">

    <div class="col-md-8">

        <h3 class="my-3">Co od tohoto webu očekávat?

        </h3>

        <p>Budu se Vám snažit přiblížit svůj prozatímní život v nových souvislostech. Vzhledem k tomu, že se stále učím, bude se tento web také stále vylepšovat. Dále má také za úkol shromáždit všechny mé již dokončené projekty a zájmy.</p>





    </div>

    <div class="col-md-4">

        <h3 class="my-3">Proč to dělám?</h3>

        <p>Mým koníčkem je právě Informační technologie. Zkrátka a dobře mě to baví a většinu dělám se svém volném čase.</p>





    </div>



</div>





<!-- Related Projects Row -->

<h3 class="my-4">Moje nedávné projekty</h3>


<div class="row">


    <div class="align-self-center col-md-3 col-sm-6 mb-4">

        <a data-fancybox="gallery" href="Img/projekty/prj1.jpg">

        <img class="img-fluid" src="Img/projekty/prj1.jpg" alt="fotka1">

        </a>

    </div>


    <div class="align-self-center col-md-3 col-sm-6 mb-4">

        <a data-fancybox="gallery" href="Img/projekty/prj2.jpg">

        <img class="img-fluid" src="Img/projekty/prj2.jpg" alt="fotka2">

        </a>

    </div>


    <div class="align-self-center col-md-3 col-sm-6 mb-4">

        <a data-fancybox="gallery" href="Img/projekty/prj3.jpg">

        <img class="img-fluid" src="Img/projekty/prj3.jpg" alt="fotka3">

        </a>

    </div>


    <div class="align-self-center col-md-3 col-sm-6 mb-4">

        <a data-fancybox="gallery" href="Img/projekty/prj4.jpg">

        <img class="img-fluid" src="Img/projekty/prj4.jpg" alt="fotka4">

        </a>

    </div>


</div>